package com.org.springboot.web.limitsservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.springboot.web.limitsservice.bean.LimitConfig;
import com.org.springboot.web.limitsservice.configuration.Configuration;

@RestController
public class LimitServiceController {
	
	@Autowired
	private Configuration config;

	@GetMapping("/limits")
	public LimitConfig retriveLimitsFromConfig(){
		
		return new LimitConfig(config.getMinimum(),config.getMaximum());
	}
	
	
}
